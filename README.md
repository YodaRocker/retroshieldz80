# RetroShield Z80 for Arduino Mega

![RetroShield in Action](./docs/images/RetroShieldZ80_in_action.jpg)

I'm excited to announce a very cool project for retromaniacs:

* RetroShield Z80 is a hardware shield for Arduino Mega. 
* It enables Arduino to emulate peripherals and run Z80 programs using a real Z80! 
* You can try Z80 assembly and run some of the old Z80 programs 
without building complicated circuits. 
* You can use existing Arduino shields to extend the capabilities. 
* Arduino Mega gives you 4~6KB of RAM and >200KB ROM (in flash area).
* All hardware and software files are provided for you to study and play. 
* Visit http://www.8bitforce.com/ if you like to purchase PCB's.


### Z80 Links

* http://www.z80.info/
* SB-Assembler: https://www.sbprojects.net/sbasm/

### License Information

* Copyright 2019 Erturk Kocalar, 8Bitforce.com
* Hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
* Software is released under the [MIT License](http://opensource.org/licenses/MIT).
* Distributed as-is; no warranty is given.
* See LICENSE.md for details.
* All text above must be included in any redistribution.
